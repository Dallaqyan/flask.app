from flask import Flask, render_template, request, jsonify, make_response

import psycopg2

app = Flask(__name__)
try:
    conn = psycopg2.connect("dbname='Contacts' user='postgres' host='localhost' password='000000'")
except:
    print("I am unable to connect to the database")
cur = conn.cursor()

@app.route('/',methods = ['GET', 'POST', 'delete'])
def home_page():
    if request.method == 'POST':
        user_name = request.form['user_name']
        phone_number = request.form['phone_number']
        query = f'INSERT INTO Contacts(user_name, phone_number) VALUES(\'{user_name}\', {phone_number});'
        cur.execute(query)
        conn.commit()

    if request.method == "DELETE":
        name = request.args.get('name')
        print(name)
        cur.execute("DELETE FROM students WHERE user_name=%s", (user_name))
        conn.commit()
        return make_response(jsonify(success="success"), 200)

    else:
        search = request.args.get('search')
        if search is None:
            cur.execute("""SELECT * FROM contacts""")
        else:
            query = f'SELECT * FROM contacts WHERE user_name LIKE \'%{search}%\';'
            cur.execute(query)
        rows = cur.fetchall()

        return render_template('home_page.html', contacts=rows)

