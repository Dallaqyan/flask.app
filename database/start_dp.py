import psycopg2

try:
    conn = psycopg2.connect("dbname='Contacts' user='postgres' host='localhost' password='000000'")
except:
    print("I am unable to connect to the database")
cur = conn.cursor()
sql_file = open("schema.sql")
sql_string = sql_file.read()
print(sql_string)

cur.execute(sql_string)
conn.commit()
